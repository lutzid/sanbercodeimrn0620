// Soal No. 1 (Range)
function range(startNum, finishNum) {
    var numbers = []
    if(startNum < finishNum) {
        for(var i = startNum; i <= finishNum; i++) {
            numbers.push(i)
        }
    }
    else if(startNum > finishNum) {
        for(var i = startNum; i >= finishNum; i--) {
            numbers.push(i)
        }
    } 
    else {
        return -1
    }
    return numbers
}

console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50))
console.log(range()) 

// Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var numbers = []
    if(startNum < finishNum) {
        for(var i = startNum; i <= finishNum; i += step) {
            numbers.push(i)
        }
    }
    else if(startNum > finishNum) {
        for(var i = startNum; i >= finishNum; i -= step) {
            numbers.push(i)
        }
    } 
    else {
        return -1
    }
    return numbers
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step = 1) {
    var numbers = []
    var res = 0

    if(finishNum) {
        numbers = rangeWithStep(startNum, finishNum, step)
        for(var i = 0; i < numbers.length; i++) {
            res += numbers[i]
        }
    }
    else if(startNum) {
        res = startNum
    }
    else {
        res = 0
    }
    return res;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)
function dataHandling(data) {
    var str = ''
    for(var i = 0; i < data.length; i++) {
        str += `Nomor ID:  ${data[i][0]}\nNama Lengkap:  ${data[i][1]}\nTTL:  ${data[i][2]}\nHobi:  ${data[i][3]}\n\n`
    }
    return str
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log(dataHandling(input))

// Soal No. 5 (Balik Kata)
function balikKata(data) {
    var str = ''
    for(var i = data.length-1; i >= 0; i--) {
        str += data[i]
    }
    return str
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal No. 6 (Metode Array)
function getMonth(data) {
    var month = ''
    switch(data) {
        case '01': month = "Januari"; break;
        case '02': month = "Februari"; break;
        case '03': month = "Maret"; break;
        case '04': month = "April"; break;
        case '05': month = "Mei"; break;
        case '06': month = "Juni"; break;
        case '07': month = "Juli"; break;
        case '08': month = "Agustus"; break;
        case '09': month = "September"; break;
        case '10': month = "Oktober"; break;
        case '11': month = "November"; break;
        case '12': month = "Desember"; break;
    }
    return month
}

function dataHandling2(data) {
    data.splice(1, 1, data[1] + ' Elsharawy')
    data.splice(2, 1, 'Provinsi ' + data[2])
    data.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(data)
    
    var dob = data[3].split('/')
    console.log(getMonth(dob[1]))

    var sorteddob = dob.sort(function (value1, value2) { return value2 - value1 })
    console.log(sorteddob)

    var dashdob = data[3].split('/').join('-')
    console.log(dashdob)

    var name = data[1].slice(0, 14)
    console.log(name)
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"] 
dataHandling2(input)