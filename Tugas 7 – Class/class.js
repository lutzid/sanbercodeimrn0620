// Soal No 1. Animal Class 
// Release 0
class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
    get name() {
        return this._name
    }
    get legs() {
        return this._legs
    }
    get cold_blooded() {
        return this._cold_blooded
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal{
    constructor(name) {
        super(name)
        this._legs = 2
    }
    yell() {
        console.log('Auooo')
    }
} 
class Frog extends Animal{
    constructor(name) {
        super(name)
        this._cold_blooded = true
    }
    jump() {
        console.log('hop hop')
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// Soal No 2. Function to Class
class Clock {
    constructor({template}) {
        this.template = template
    }
    render() {
        this.date = new Date();
        this.hours = this.date.getHours();
        this.mins = this.date.getMinutes();
        this.secs = this.date.getSeconds();
        
        if (this.hours < 10) this.hours = '0' + this.hours;
        if (this.mins < 10) this.mins = '0' + this.mins;
        if (this.secs < 10) this.secs = '0' + this.secs;

        this.output = this.template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);
        console.log(this.output)
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  