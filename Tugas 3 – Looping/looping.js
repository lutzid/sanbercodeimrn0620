// Soal No. 1 Looping While
console.log('LOOPING PERTAMA')
var i = 2
while(i <= 20) {
    console.log(i + ' - I love coding')
    i += 2
}

console.log('LOOPING KEDUA')
while(i > 2) {
    i -= 2
    console.log(i + ' - I will become a mobile developer')
}

// Soal No. 2 Looping menggunakan for
console.log('OUTPUT')
for(var i = 1; i <= 20; i++) {
    if(i % 3 === 0 && i % 2 === 1){
        console.log(i + ' - I Love Coding')
    }
    else if (i % 2 === 1) {
        console.log(i + ' - Santai')
    }
    else if (i % 2 === 0) {
        console.log(i + ' - Berkualitas')
    }
}

// Soal No. 3 Membuat Persegi Panjang #
for(var i = 0; i < 4; i++) {
    var line = ''
    for(var j = 0; j < 8; j++) {
        line += '#'
    }
    console.log(line)
}

// Soal No. 4 Membuat Tangga
for(var i = 0; i < 7; i++) {
    var line = ''
    for(var j = 0; j < 7; j++) {
        if(j <= i) {
            line += '#'
        }
    }
    console.log(line)
}

// Soal No. 5 Membuat Papan Catur
for(var i = 0; i < 8; i++) {
    var line = ''
    for(var j = 0; j < 8; j++) {
        if(i % 2 === 0) {
            if(j % 2 === 0){
                line += ' '
            }
            else {
                line += '#'
            }
        } else {
            if(j % 2 === 0){
                line += '#'
            }
            else {
                line += ' '
            }
        }
    }
    console.log(line)
}