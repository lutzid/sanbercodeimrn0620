// Soal No. 1 (Array to Object)
function getAge(birth_year) {
    var thisYear = new Date().getFullYear()
    if(isNaN(birth_year)){
        return "Invalid Birth Year"
    }
    return (birth_year > thisYear ? "Invalid Birth Year" : thisYear - birth_year )
}

function arrayToObject(arr) {
    if(arr === []) return
    var arrObj = {}
    for(var i = 0; i < arr.length; i++) {
        arrObj.firstName = arr[i][0]
        arrObj.lastName = arr[i][1]
        arrObj.gender = arr[i][2]
        arrObj.age = getAge(arr[i][3])
        console.log(`${i + 1}. ${arrObj.firstName} ${arrObj.lastName}: `, arrObj)
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
function shoppingTime(memberId, money) {
    var purchasedItems = []
    var change = money
    var items = [
        ['Sepatu Stacattu', 1500000],
        ['Baju Zoro', 500000],
        ['Baju H&N', 250000],
        ['Sweater Uniklooh', 175000 ],
        ['Casing Handphone', 50000]
    ]
    if(!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    }
    else if(money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    } else {
        for(var i = 0; i < items.length; i++) {
            if(change >= items[i][1]) {
                purchasedItems.push(items[i][0])
                change -= items[i][1]
            } else continue
        }
        var summary = {
            memberId: memberId,
            money: money,
            listPurchased: purchasedItems,
            changeMoney: change
        }
        return summary
    }
}
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(listPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var summary = []
    for(var i = 0; i < listPenumpang.length; i++) {
        var price = 2000 * (rute.indexOf(listPenumpang[i][2]) - rute.indexOf(listPenumpang[i][1]))
        var obj = {
            penumpang: listPenumpang[i][0],
            naikDari: listPenumpang[i][1],
            tujuan: listPenumpang[i][2],
            bayar: price
        }
        summary.push(obj)
    }
    return summary
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]