import React, { Component } from 'react';
import {
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TouchableOpacity, 
    ImageBackground,
    FlatList,
} from 'react-native';
import bgImage from './assets/background.png'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/MaterialIcons'
import Skills from './components/Skills'
import data from './skillData.json'

export default class SkillScreen extends Component {
    render () {
        return (
            <ImageBackground source={bgImage} style={styles.backgroundContainer}>
                <View style={styles.body}>
                    <View style={styles.profile}>
                    <Image source={{uri: 'https://randomuser.me/api/portraits/men/0.jpg'}} style={{width: 110, height:110, borderRadius: 100, marginBottom: 10}}/>
                        <Text style={{color: '#FFFFFF', fontSize: 24, marginBottom: 30}}>Zidan Lutfi</Text>
                    </View>
                    <View style={styles.card}>
                        <View style={styles.itemContainer}>
                            <FlatList
                                data={data.items}
                                renderItem={(skill) => <Skills skill={skill.item}/>}
                                keyExtractor={(item) => item.id}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon2 name="account-circle" size={25}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon2 name="code" size={25} style={{color: '#2CABF3'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon2 name="settings" size={25} />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    backgroundContainer: {
        flex: 1,
        width: null,
        height: null,
    },
    profile: {
    },
    body: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        width: 320,
        borderRadius: 16,
        borderRightColor: '#000000',
        elevation: 3
    },
    itemContainer: {
        margin: 40,
        flex: 1,
        flexDirection: 'column',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    item: {
        width: 120,
        alignItems: 'center',
        padding: 10,
        flexDirection: 'row'
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    tabItem: {
      alignItems: 'center',
      justifyContent: 'center',
      margin: 25
    },
    itemDesc: {
        
    }
});