// Soal No. 2 (Promise Baca Buku)
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var timeRemain = 10000
var idx = 0

readBooksPromise(timeRemain, books[idx])
    .then(function call(time) {
        idx++
        if(idx == books.length) return
        else {
            readBooksPromise(time, books[idx]).then(call)
            .catch(function errCatch(error) {
                //some code here
            })
        }
    }).catch(function errCatch(error) {
        //some code here
    })