// Soal No. 1 (Callback Baca Buku)
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var timeRemain = 10000
var idx = 0

readBooks(timeRemain, books[idx], function callback(time){
    idx++
    if(idx == books.length) return
    else readBooks(time, books[idx], callback)
})