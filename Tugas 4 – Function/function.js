// Soal No. 1 Return Halo Sanbers
function teriak() {
    return 'Halo Sanbers!'
}
 
console.log(teriak())

// Soal No. 2 Return Multiply
function kalikan(num1, num2) {
    return num1 * num2
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(num1, num2)
console.log(hasilkali)

// Soal No. 3 Return Introduction
function introduce(name, age, address, hobby) {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
}

var name = "Zidan"
var age = 19
var address = "Indonesia"
var hobby = "Coding"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)